
var express = require("express");
var path = require("path");
var logger = require("morgan");
var bodyParser = require("body-parser");
var neo4j = require("neo4j-driver");
const { restart } = require("nodemon");
//Necesarios para iniciar sesion en neo4j
var driver;
var session;
//Dependiendo de el nombre habra diferentes opciones
var username;

var app = express();

//View engine 
app.set('views' , path.join(__dirname , 'views'));
app.set('view engine' , 'ejs');

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));
app.use(express.static(path.join(__dirname , 'public')));

app.post("/logged/process" , function(req , res){
    
    try {
        username = req.body.user;
        var password = req.body.password;
        driver = neo4j.driver("bolt://localhost:7687" , neo4j.auth.basic(username , password));
        session = driver.session();
        session.run("MATCH (n) return n")
        .then(function(result){
        })
        .catch(function(err){
        });
    } catch (error) {
        alert("Algo ha ido mal");
        console.log(error);
    }
    if(username == "neo4j" && password=="password"){
        res.redirect("/redirect/PersonForm");
    }
    else if(username == "rubenGutierrez" && password=="123456"){
        res.redirect("/redirect/PersonForm");
    }else if(username == "arquitect" && password=="123"){
        res.redirect("/redirect/PersonForm");
    }else if(username == "publisher" && password=="pass"){
        res.redirect("/redirect/PersonForm");
    }
    
});


app.get("/" , function(req , res){
    res.render('log');

});

app.get("/redirect/PersonForm" , function(req , res){
    res.render('PersonForm');

});

app.get("/redirect/removeElements" , function(req , res){
    res.render('removeElements');

});

app.get("/redirect/CompanyForm" , function(req , res){
    res.render('CompanyForm');

});

app.get("/redirect/viewGraph" , function(req , res){
    res.render('viewGraph');

});

app.get("/redirect/RelationForm" , function(req , res){
    session
        .run("MATCH(n:Person) RETURN n")
        .then(function(result){
            var personArr = [];
            var compArr = [];


            result.records.forEach(function(record){
                personArr.push({
                    name: record._fields[0].properties.name
                });
            });

            session
            .run("MATCH(n:Company) RETURN n")
            .then(function(result){
                result.records.forEach(function(record){
                    compArr.push({
                        name: record._fields[0].properties.name
                    });
                });

                res.render('createRelation' , {
                    person : personArr,
                    company : compArr
                });
            })
            .catch(function(err){
                
            })


        })
        .catch(function (err){
            console.log(err);
            res.redirect("/")

        });

});

app.get("/logged-reader" , function(req , res){
    session
        .run("MATCH(n:Person) RETURN n LIMIT 25")
        .then(function(result){
            var personArr = [];
                result.records.forEach(function(record){
                    personArr.push({
                        name: record._fields[0].properties.name
                    });
            });
            res.render('reader' , {
                person : personArr
            });
        })
        .catch(function (err){
            console.log(err);
            res.redirect("/logged-reader");

        });
});


app.get("/logged-admin" , function(req , res){
    session
        .run("MATCH(n:Person) RETURN n LIMIT 25")
        .then(function(result){
            var personArr = [];
                result.records.forEach(function(record){
                    personArr.push({
                        name: record._fields[0].properties.name
                    });
            });
            res.render('index' , {
                person : personArr
            });
        })
        .catch(function (err){
            console.log(err);
            res.redirect("/redirect/PersonForm");
        });
});


//Metodo para añadir personas

app.post("/person/add" , function(req , res){

        var name =  req.body.personName;
        var last = req.body.personLast;
        var dni = req.body.personDni;
        var country = req.body.personCountry;
        var phone = req.body.personPhone;
        var sex = req.body.personSex;
    
        console.log(name)
        console.log(last)
        console.log(dni)
        console.log(country)
        console.log(phone)
        console.log(sex)
    
        session
            .run("CREATE (p:Person {name: $nameParam , lastName : $paramLast , dni : $paramDni , country : $paramCountry , phone : $paramPhone , sex : $paramSex })RETURN p" , 
            {nameParam : name , paramLast : last ,paramDni : dni ,paramCountry : country ,paramPhone : phone ,paramSex : sex})
            .then(function(result){
                res.redirect("/redirect/PersonForm");
            })
            .catch(function(err){
                console.log(err);
                alert("Ha ocurrido un error");
                res.render('PersonForm');
            });


});

//Metodo para añadir empresas
app.post("/company/add" , function(req , res){
    var name = req.body.compName;
    var country = req.body.compCountry; 
    var id = req.body.compId;

    session
        .run("CREATE (comp:Company {name: $nameParam , country : $countParam , id : $idParam})RETURN comp" , {nameParam : name , countParam : country , idParam : id})
        .then(function(result){
            res.render('CompanyForm');
        })
        .catch(function(err){
            console.log(err);
            alert("Ha ocurrido un error");
            res.render('CompanyForm');
        });

});


//Metodo para eliminar personas segun su nombre
app.post("/person/remove" , function(req , res){
    var dni = req.body.dni;
    session
        .run("MATCH (n:Person {dni: $nameParam}) DETACH DELETE n" , {nameParam : dni})
        .then(function(result){
            res.redirect("/redirect/removeElements");
        })
        .catch(function(err){
            console.log(err);
        });

});

app.post("/company/remove" , function(req , res){
    var id = req.body.id;
    session
        .run("MATCH (n:Company {id: $nameParam}) DETACH DELETE n" , {nameParam : id})
        .then(function(result){
            res.redirect("/redirect/removeElements");
        })
        .catch(function(err){
            console.log(err);
        });

});

app.post("/person/relation" , function(req , res){


    var company = req.body.company;
    var person = req.body.person;
    var type = req.body.type;

    console.log(company)
    console.log(person)
    console.log(type)

    session
        .run("MATCH (o), (c) WHERE o.name = $name1Param AND c.name = $name2Param CREATE (o)-[R:"+ type + "]->(c)" , {name1Param : person  , name2Param : company})
        .then(function(result){
            res.redirect("/redirect/PersonForm");
        })
        .catch(function(err){
            console.log(err);
        });
});

app.post("/person/removeDb" , function(req , res){
    session
        .run("MATCH (a)-[rel]->(b) DELETE rel")
        .then(function(result){
                session
                .run("MATCH (a) delete a")
                .then(function(result){
                    res.redirect("/redirect/PersonForm");
                })
                .catch(function(err){
                    console.log(err);
                });
        })
        .catch(function(err){
            console.log(err);
        });


});

app.listen(3005);
console.log("Server started on port 3005");

module.exports = app;

//Añadir metodo para hacer query sobre el grafo